# Demo
https://webrtc-multiple.herokuapp.com/demos/

# Install
git clone https://gitlab.com/matiasrivera/rtcmultiple.git

npm install

# initialize
node server.js --ssl


## Wiki Pages

1. https://github.com/muaz-khan/RTCMultiConnection/wiki
2. https://github.com/muaz-khan/RTCMultiConnection-Server/wiki

## License

[RTCMultiConnection](https://github.com/muaz-khan/RTCMultiConnection) is released under [MIT licence](https://github.com/muaz-khan/RTCMultiConnection/blob/master/LICENSE.md) . Copyright (c) [Muaz Khan](https://MuazKhan.com/).
